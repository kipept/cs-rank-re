#include <amxmodx>
#include <amxmisc>
#include <hamsandwich>
#include <fun>
#include <sqlx>
#include <fakemeta>
#include <dhudmessage>
#include <sockets>
#include <colorchat>
#include <cstrike>

#pragma dynamic 32768 

/** 
	Автор и название плагина
 */

#define PLUGIN "CSRankRe"
#define AUTHOR "RevCrew"

/** 
	Версия плагина и версия торговой площадки
 */
new const VERSION[] = "2020.0"
new const MARKET_VERSION[] = "2.0"

// API
new const API_VERSION[] = "1.0.1"
new const API_VERSION_NUM = 101

// Thanks to Mistrick and his Weapon Skin System
#include <weapon_models_api.inc>

#define DEBUG

/** 
	Глобальные переменные
 */
#include <CSRankRe/globals.inc>

/** 
	Общие функции для нескольких плагинов
 */
#include <CSRankRe/tools>

/** 
	Шансы для различного дропа
 */
#include <CSRankRe/chance>

/** 
	Общие функции для этого плагина
 */
#include <CSRankRe/stock>

/** 
	Операции с предмета игрока
 */
#include <CSRankRe/operations.inc>

/** 
	Модуль опыта и уровней
 */
#include <CSRankRe/core_exp_level>

/** 
	Модуль для работы с валютами
 */
#include <CSRankRe/stats.inc>

/** 
	Весь sql тута
 */
#include <CSRankRe/sql.inc>

/** 
	Модуль работы со скинами
 */
#include <CSRankRe/skins.inc>

/** 
	Модуль для операций над игроком
 */
#include <CSRankRe/player.inc>

/** 
	Информер
 */
#include <CSRankRe/hud.inc>

/** 
	Консольные команды
 */
#include <CSRankRe/commands.inc>

/** 
	Торговая площадка
 */
#include <CSRankRe/marketplace.inc>

/** 
	Ежедневные награды
 */
#include <CSRankRe/rewards.inc>

/** 
	API
 */
#include <CSRankRe/native.inc>

/** 
	Эвенты
 */
#include <CSRankRe/events.inc>
//#include <CSRankRe/stattrack.inc>

/** 
	Некоторые меню
 */
#include <CSRankRe/menu.inc>

public plugin_precache() {
	/**
		При старте плагина создаем папку с логами, если ее нету
	 */
	createDirByType(DIR_LOG)

	/**
		Массивы для скинов
	 */
	g_Items = ArrayCreate(Items);
	g_ItemsTrie = TrieCreate();

	/**
		Регистрируем квары
	 */
	register_cvars()

	/**
		Читаем скины из файла и прекешем их
	 */
	ReadFile_Skins();
	PrecacheSkins();

	/**
		Инициируем модули
	 */
	CoreExp_Init()
	Market_init()
	Rewards_Init()
	Events_Init()

	precache_sound(g_szSoundLevelUp)
	precache_sound(g_szSoundMedalUp)
}


public plugin_init() {
	register_plugin(PLUGIN, VERSION, AUTHOR);
	register_dictionary("CSRankRe.txt")

	register_event( "DeathMsg", "Event_DeathMsg", "a" );
	register_clcmd("CSR_SetMoney", "SetMPMoney");
	
	static const COMMAND_MENU[] = "ShowMainMenu"
	static const COMMAND_RANK[] = "ShowRank"

	/**
		Команды для открытия меню
	*/

	register_clcmd("say /csre", COMMAND_MENU)
	register_clcmd("say_team /csre", COMMAND_MENU)

	/**
		Команды для показа ранка игрока
	*/

	register_clcmd("say /csrank", COMMAND_RANK)
	register_clcmd("say_team /csrank", COMMAND_RANK)

	/**
		Общая консольная команда для выдачи игроку всего(для админов)
	*/
	register_concmd("csre_add_feature", "CSRAddFeature", cvar_flags(g_Cvars[CVAR_MAIN_FLAG]), "Use: <name or #userid> <type:exp,coin,case,key,skin_id,smart_id> <number or id>")

	/**
		Инициируем SQL
	*/
	SqlX();

	//Register Spawn
	RegisterHam( Ham_Spawn, "player", "FwdPlayerSpawn", 1 );
	
	g_HudSync = CreateHudSyncObj();

	/**
		Отображение название скинов и тд у игроков	
	*/
	set_task(2.0, "TaskDisplayInfo", 1337,_,_,"b")

	/**
		Выдача предметов по времени
	*/
	set_task( 300.0 , "TaskMainLoop", 1338,_,_,"b")
}

public plugin_cfg() {
	/**
		Очищаем если скопилось много лог файлов
	*/
	ClearLogs();

	g_Event = Events_GetCurrentEvent();
}

public plugin_end() {
	/**
		Аоаоаао
	*/
	CoreExp_end();
	Market_end();
	Rewards_end();
	Events_end();

	Skins_end();
	TrieDestroy(g_ItemsTrie)
}

public client_authorized(id) {
	/**
		Обнуляем игрока и получаем его данные
	*/
	Player_Reset(id)

	get_user_name(id, g_player_data[id][PD_NAME], 31)
	get_user_ip(id, g_player_data[id][PD_IP], 25, 1)
	get_user_authid(id, g_player_data[id][PD_AUTHID], 25)

	if(!is_valid_steamid(g_player_data[id][PD_AUTHID]))
		formatex(g_player_data[id][PD_AUTHID], 25, "ERROR_STEAM%d%d",random_num(1,1000), random_num(1,1500))

	if(is_user_bot(id) || is_user_hltv(id))
		return;

	/**
		Получаем данные об игроке из БД
	*/
	Sql_GetPlayer(id, g_player_data[id][PD_AUTHID], g_player_data[id][PD_IP])

	/**
		Устаревшая штука.
	*/
	g_vip[id] = check_player_vip(id)
}

public client_disconnect(id) {
	/**
		Сохраняем игрока при выходе из игры
	*/
	Player_Save(id)
}

public TaskMainLoop()
{
	new p[32],c, player;
	get_players(p,c, "ch")

	if(!c) return;

	new chance = 0;
	for(new i; i<c; i++)
	{
		player = p[i];
		
		if(get_user_time(player) < 600 || g_take_item[player])
			continue;


		// Выдаем предметы если игрок больше 10 минут на сервере и он не получал еще предметы
		chance = CalculateDropTimeChance()
		if(random_num(1,100) <= chance)
		{
			chance = get_pcvar_num(g_Cvars[CVAR_TIME_KEYS_CHANCE])

			if (random_num(1,100) <= chance) 	Stats_AddPlayerKeys(player, 1)
			else 								GiveItem(player, false, false)

			g_take_item[player] = true;
		}
			
	}
}

public TaskDisplayInfo()
{
	if( !get_pcvar_num(g_Cvars[CVAR_INFORMER]) )
		return;

	new p[32],c, player, id;
	get_players(p,c, "ch")

	new iWeaponID;
	new iWeaponSkin;
	new color[3];

	new sOwnerName[32];
	new sPlayerName[32];

	new data[Items];
	new bool:cmpName;

	for(new i; i<c; i++)
	{
		id = is_user_alive( p[ i ] ) ? p[ i ] : pev( p[ i ] , pev_iuser2 ) ;
		player = p[i];

		if(id>32 || id <= 0)
			continue;

		// Показываем информер слева вверху
		HudShow(id, player)
		
		if(!is_valid_player(id))
			continue;

		iWeaponID = get_pdata_cbase(id, 373, 5);

		if ( !pev_valid(iWeaponID) ||  !get_weapon_skin(iWeaponID) ) {
			continue;
		}

		iWeaponSkin = get_weapon_skin(iWeaponID);
		ArrayGetArray(g_Items, iWeaponSkin == ZERO_SKIN_INT ? 0 : iWeaponSkin, data)

		get_hud_color_skin(data[ITEM_CLASS], color)
	
		get_weapon_owner( iWeaponID, sOwnerName, charsmax(sOwnerName) );
		get_user_name(id, sPlayerName, charsmax(sPlayerName))
			
		/* True when another player take item */
		cmpName = bool:(!equali(sOwnerName, sPlayerName) && strlen(sOwnerName) >= 2);

		if(cmpName)
		{
			set_hudmessage(150, 150, 150, -1.0, 0.67, 0, 2.0, 2.0, _, _, 2)
			ShowSyncHudMsg(player, g_HudSync,"%s's:", sOwnerName)
		}
			
		// Показываем название скина
		set_hudmessage(color[0], color[1], color[2], -1.0, 0.7, 0, 2.0, 2.0, _, _, 2)		
		show_hudmessage(player, "%s", data[ITEM_NAME] )
		
	}

}


public Event_DeathMsg()
{
  	new iVictim, iKiller;
	iVictim = read_data( 2 );
	iKiller = read_data( 1 );
	
	if( !is_user_connected(iVictim) || !is_user_connected(iKiller))
		return;
	
	if(!iKiller )
		return;

	if (iKiller == iVictim)
		return;

	new weapon[32];read_data(4, weapon, charsmax(weapon))
	format(weapon, charsmax(weapon), "weapon_%s", weapon)

	/**
		Если кол-во играющих игроков меньше минимально указанного в кварах, то опыт не зачисляем	
	*/
	static _min_players = 0;
	if ( !_min_players) _min_players = get_pcvar_num(g_Cvars[CVAR_MIN_PLAYERS])

	if(get_playersnum() < _min_players)
	{
		Print(iKiller,"%L", iKiller, "CSRE_MIN_PLAYERS", _min_players)
		return;
	}

	CoreExp_kill(iKiller, bool:read_data(3))
	
	/**
		Показываем умершему игроку сообщение о том, скольких он убил(если он убил)
	*/
	if(g_user_kills[iVictim] > 0 )
	{
		Print(iVictim, "%L",iVictim, "CSRE_EXP_GIVE_KILL",g_user_kills[iVictim])
		g_user_kills[iVictim] = 0;
	}
}

public FwdPlayerSpawn(id) {

	if (!is_user_alive(id)) {
		return;
	}

	new team = get_user_team2(id);

	if ( g_isTeamChanged[id] && g_isTeamChanged[id] == team) {
		return ; // Игрок не менял команду, оставляем как есть
	}

	/**
		Меняем скины оружия за другую команду
	*/
	for(new i; i< WEAPON_SIZE + 1; i++) {
		if ( !g_player_weapons[id][team][i]) continue;

		set_player_weapon_skin(id, team, i, g_player_weapons[id][team][i]);
	}

	g_isTeamChanged[id] = team;
}

public ShowMainMenu(id)
{
	/**
		При заходе в меню, проверяем ежедневные награды для игрока	
	*/
	Rewards_Check(id)

	new menu, title[196];
	formatex(title, charsmax(title),"%L", id, "CSRE_MAINMENU_TITLE")
	
	if (g_Event) {
		new event[Events];
		event = Events_getEventInfo(g_Event);

		formatex(title, charsmax(title), "%s^n%L", title, id, "CSRE_EVENT_CURRENT_TITLE", event[EVENT_NAME])
	}
	 

	menu = menu_create(title, "HandleMainMenu")

	formatex(title, charsmax(title), "%L", id, "CSRE_MAINMENU_STATS")
	menu_additem(menu, title, "1")

	formatex(title, charsmax(title), "%L^n", id, "CSRE_MAINMENU_ALLSKIN", ArraySize(g_Items))
	menu_additem(menu, title, "2")
	
	formatex(title, charsmax(title), "%L", id, "CSRE_SKINS_OWN")
	menu_additem(menu, title, "3")
	
	formatex(title, charsmax(title), "%L^n", id, "CSRE_OPEN_MENU", g_player_stats[id][PS_CASES])
	menu_additem(menu, title, "4")

	formatex(title, charsmax(title), "%L", id, "CSRE_MAINMENU_SHOP")
	menu_additem(menu, title, "5")

	formatex(title, charsmax(title), "%L \w[v%s]", id, "CSRE_MARKETPLACE_TITLE",MARKET_VERSION)
	menu_additem(menu, title, "6")

	if (g_events && ArraySize(g_events)) {
		formatex(title, charsmax(title), "%L", id, "CSRE_EVENT_MAIN_TITLE")
		menu_additem(menu, title, "7")
	}
	

	// menu_addblank(menu,1);

	// menu_additem(menu, "Выход", "9")

	// menu_setprop(menu, MPROP_PERPAGE, 0)
	// menu_setprop(menu, MPROP_EXIT, MEXIT_ALL)

	MenuSetProps(id, menu);
	menu_display(id, menu, 0)
}

public HandleMainMenu(id, menu, item)
{
	if(item == MENU_EXIT)
		return menu_destroy(menu);
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new uid = str_to_num(data);
	
	switch (uid)
	{
		case 1: ShowStatsMenu(id)
		case 2: ShowAllSkinMenu(id);
		case 3: Sql_GetPlayerItems(id, "QueryLoadSkins")
		case 4: ShowContainerMenu(id);
		case 5: ShowShopMenu(id);
		case 6: Market_MainMenu(id);
		case 7: {
			if (g_events && ArraySize(g_events)) ShowMainEventsMenu(id)
		}
	}

	return menu_destroy(menu);
}

/**
	Отображаем топ 15
*/
public QueryLoadTop(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		return SQL_Error(Query, Error, Errcode, FailState);
	}
	
	new id = Data[0];
	
	if(SQL_NumResults(Query) <= 0)
		return SQL_FreeHandle(Query);
		
	new html_motd[ 2500 ],maxtop;maxtop = 1;
	new  name[32] ,exp, len;

	new style[32];
	get_pcvar_string(g_Cvars[CVAR_STYLE_TOP], style, charsmax(style))

	if (!style[0]) copy(style, charsmax(style), "default");

	len = formatex ( html_motd [ len ], charsmax ( html_motd ) - len, "<!DOCTYPE HTML><html><head><meta charset='UTF-8'><link rel=^"stylesheet^" href=^"http://cs-suite.ru/assets/CSRankRe/top/app.css^"><link rel=^"stylesheet^" href=^"http://cs-suite.ru/assets/CSRankRe/top/%s.css^"></head>", style );
	len += formatex ( html_motd [ len ], charsmax ( html_motd ) - len, "<body><div class=^"table_block^"><p align=^"left^"><font size=^"5px^">Топ лучших игроков</font></p><table><tr align=center><th width=4% align=left>#</th><th width=30% align=left>NickName</th><th width=5% align=left>Total Exp</th></tr>");
	
	new const class_q[] = "class=^"q^"";
	while( SQL_MoreResults(Query))
	{
		
		if(maxtop > 15) break;
			
		SQL_ReadResult(Query,SQL_FieldNameToNum(Query,"player_name"),name,31);
		exp = SQL_ReadResult(Query,SQL_FieldNameToNum(Query,"exp_total"));
	
		len += formatex(html_motd [ len ], charsmax(html_motd)-len, "<tr %s><td>%i.</td><td>%s</td><td>%d</td></tr>", maxtop % 2 == 0 ? class_q : " ", maxtop, name,exp);
		
		SQL_NextRow(Query);
		maxtop ++
	}

	len += formatex ( html_motd [ len ], charsmax ( html_motd ) - len, "</table></div></body></html>" )
		
	show_motd( id, html_motd, "Top CSRankRe" );	
	return SQL_FreeHandle(Query);
}	

ShowAllSkinMenu(id)
{
	new menu, title[128];
	new size = ArraySize(g_Items)
	formatex(title, charsmax(title),"%L ^n %L",\
	 id, "CSRE_MAINMENU_ALLSKIN", size, id, "CSRE_NOT_GET_FROM_CONTAINER")
	
	menu = menu_create(title, "HandleAllSkinMenu")
	
	new data[Items], pid[10], temp[128];

	for(new i = 0; i < size; i++)
	{
		ArrayGetArray(g_Items, i, data)
		num_to_str(i, pid, charsmax(pid))
	
		formatex(temp, charsmax(temp), "%s%s%s %s", (data[ITEM_CLASS] == 3) ? "\y" : data[ITEM_CLASS] == 2 ? "\r" : "\w",data[ITEM_NAME],
		!data[ITEM_GET_FROM_CONTAINER] ? "\r*" : "",
		( Events_isValidEvent(data[ITEM_EVENT]) && data[ITEM_EVENT] == g_Event) ? "\y[E]" : ( Events_isValidEvent(data[ITEM_EVENT]) ? "\d[E]" : ""))
		menu_additem(menu, temp, pid);
	}
	
	menu_setprop(menu, MPROP_PERPAGE, 6)
	MenuSetProps(id, menu);
	menu_display(id, menu, 0)
}

/**
	Информация о всех скинах
*/
public HandleAllSkinMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowMainMenu(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new pid = str_to_num(data);

	new _data[Items];
	ArrayGetArray(g_Items, pid, _data)

	/**
		Если скин участвует в эвентах, то выводим их названия	
	*/
	new events[192];
	if ( Events_isValidEvent(_data[ITEM_EVENT]) ) {
		add(events, charsmax(events), " | ")

		new event[Events];
		event = Events_getEventInfo( _data[ITEM_EVENT] )

		formatex(events, charsmax(events), "%s%s ", events, event[EVENT_NAME])
	}

	/**
		Выводим инфу о скине
	*/
	Print(id,"^3%s^1 | ^4%L^1%s",_data[ITEM_NAME], id, ITEM_CLASS_NAME[_data[ITEM_CLASS]-1], events)
	
	ShowAllSkinMenu(id)
	return menu_destroy(menu);
}

public QueryLoadSkins(id, Array:playerItems, player) {
	/**
		Отображаем игроку его скины
	*/
	if (player == id) player = id;

	if ( !ArraySize(playerItems) ) {
		ArrayDestroy(playerItems);
		return Print(id, "%L", id, "CSRE_MENU_EMPTY_SKINS")
	}

	new TitleType[128];
	formatex(TitleType, charsmax(TitleType), "%L", id, "CSRE_SKINMENU_TITLE")

	new menu, title[256];
	formatex(title, charsmax(title),"%L^n%s",id, "CSRE_SKINS_OWN", TitleType)
	menu = menu_create(title, "HandleSkinMenu")

	MenuSetProps(id, menu);

	new size = ArraySize(playerItems), item;
	
	new index, wid;
	new data[Items]
	new array[WEAPON_SIZE][3];

	new skinStr[SKIN_STR_LEN];

	new event[Events];
	if (g_Event) {
		event = Events_getEventInfo(g_Event)
	}

	for(new i; i< size; i++) {
		item = ArrayGetCell(playerItems, i);

		index = search_array(item);

		if(index < 0) continue;

		ArrayGetArray(g_Items, index, data)
		wid = get_weapon_csw(data[ITEM_REPLACE]);

		/**
			Ставим красные звездочки, если скин используется игроком
		 */
		if ( isWeaponSkinOnPlayer(id, RE_TEAM_T, wid, item) ) {
			array[wid][RE_TEAM_T] = item;
		}

		if ( isWeaponSkinOnPlayer(id, RE_TEAM_CT, wid, item) ) {
			array[wid][RE_TEAM_CT] = item;
		}

		// Получаем название скина в зависимости от его редкости
		skinClassStr(data, title, charsmax(title))

		formatex(title, charsmax(title),"%s %s%s%s", title,
		( Events_isValidEvent(data[ITEM_EVENT]) && data[ITEM_EVENT] == g_Event) ? "\y[E]" : ( Events_isValidEvent(data[ITEM_EVENT]) ? "\d[E]" : ""),
		(isItemsEquals(array[wid][RE_TEAM_T], item)) ? "\r[T]\w" : "",
		(isItemsEquals(array[wid][RE_TEAM_CT], item)) ? "\r[CT]\w" : "");
		
		num_to_str(item, skinStr, SKIN_STR_LEN - 1)
		menu_additem(menu, title, skinStr)
	}

	ArrayDestroy(playerItems);
	return menu_display(id, menu, 0)
}
public HandleSkinMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowMainMenu(id)
		return menu_destroy(menu);
	}
	
	new data[SKIN_STR_LEN + 2], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), _dummy, charsmax(_dummy), callback);

	new item = str_to_num(data)

	/**
		Показываем меню о том, что игрок может надеть скин на команды
	*/
	ShowSkinMenu(id, item);
	return menu_destroy(menu);
}

ShowSkinMenu(id, item) {
	new title[128];

	new data[Items];
	ArrayGetArray(g_Items, search_array(item), data)

	skinClassStr(data, title, charsmax(title))
	new wid = get_weapon_csw(data[ITEM_REPLACE]);

	new menu;
	menu = menu_create(title, "HandleSkinSettingsMenu")

	MenuSetProps(id, menu);

	new bool:t = isWeaponSkinOnPlayer(id, RE_TEAM_T, wid, item)
	new bool:ct = isWeaponSkinOnPlayer(id, RE_TEAM_CT, wid, item)

	new skinStr[SKIN_STR_LEN];
	num_to_str(item, skinStr, SKIN_STR_LEN - 1)
	
	new itemStr[SKIN_STR_LEN + 2];
	formatex(itemStr, charsmax(itemStr), "%s#1", skinStr);

	formatex(title, charsmax(title), "%L", id, "CSRE_MENU_SKIN_EQUIP")
	formatex(itemStr, charsmax(itemStr), "%s#1", skinStr);
	menu_additem(menu, title, itemStr)

	formatex(title, charsmax(title), "%L: %sTT Team", id, "CSRE_MENU_SKIN_EQUIP", t ? "\y" : "\d")
	formatex(itemStr, charsmax(itemStr), "%s#2", skinStr);
	menu_additem(menu, title, itemStr)

	formatex(title, charsmax(title), "%L: %sCT Team", id, "CSRE_MENU_SKIN_EQUIP", ct ? "\y" : "\d")
	formatex(itemStr, charsmax(itemStr), "%s#3", skinStr);
	menu_additem(menu, title, itemStr)

	menu_display(id, menu, 0)
}

public HandleSkinSettingsMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowMainMenu(id)
		return menu_destroy(menu);
	}
	
	new data[SKIN_STR_LEN + 2], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), _dummy, charsmax(_dummy), callback);

	/**
		Немного лайфхаков
	*/
	if (strlen(data) < 3) {

		ShowMainMenu(id)
		return menu_destroy(menu);
	}

	replace(data, charsmax(data), "#", " ");

	new skinStr[SKIN_STR_LEN];
	new number[6];

	parse(data, skinStr, charsmax(skinStr), number, charsmax(number));

	new item = str_to_num(skinStr)

	new _data[Items];
	ArrayGetArray(g_Items, search_array(item), _data)

	new wid = get_weapon_csw(_data[ITEM_REPLACE]);
	new bool:isPrint = false;

	switch (str_to_num(number)) {
		case 1: {
			
			for(new team = RE_TEAM_T; team <= RE_TEAM_CT; team++) {
				if (!isPrint) {
					isPrint = isWeaponSkinOnPlayer(id, team, wid, item);
				}

				isWeaponSkinOnPlayer(id, team, wid, item) ? Operation_ResetPlayerWeaponSkin(id, wid, team) : Operation_SetPlayerWeaponSkin(id, wid, team, item)
			}
			
			if (isPrint)	Print(id, "%L", id, "CSRE_RESET_SKINS")
		}

		case 2: {
			isPrint = isWeaponSkinOnPlayer(id, RE_TEAM_T, wid, item)
			isPrint ? Operation_ResetPlayerWeaponSkin(id, wid, RE_TEAM_T) : Operation_SetPlayerWeaponSkin(id, wid, RE_TEAM_T, item)
			if (isPrint)	Print(id, "%L", id, "CSRE_RESET_SKINS")
		}
			
		case 3: {
			isPrint = isWeaponSkinOnPlayer(id, RE_TEAM_CT, wid, item)
			isPrint ? Operation_ResetPlayerWeaponSkin(id, wid, RE_TEAM_CT) : Operation_SetPlayerWeaponSkin(id, wid, RE_TEAM_CT, item)
			if (isPrint)	Print(id, "%L", id, "CSRE_RESET_SKINS")
		}
	}

	ShowMainMenu(id)
	return menu_destroy(menu);
}

/**
	из weapon_models_api.inc
*/
public cs_weapon_add_to_player(id, weapon, weaponid, type)
{
	if(type == ADD_BY_ARMORY_ENTITY || type == ADD_BY_WEAPONBOX) return 0;
	if(get_weapon_skin(weapon)) return 0;

	new team = get_user_team2(id);
	new item = get_player_weapon_skin(id, team, weaponid)
	
	if( item )
	{	
		new index = search_array(item);
	
		//server_print("[%d][Take]Index=%d,wid=%d, st=%d", id, index,weaponid,g_player_weapons[id][team][weaponid])
		if(index < 0)	return 0;

		if (!index) set_weapon_skin(weapon, ZERO_SKIN_INT);
		else		set_weapon_skin(weapon, index);
		
		new name[32];
		get_user_name(id,name,charsmax(name))

		set_weapon_owner(weapon, name);
	}

	return 0;
}
public cs_weapon_deploy(id, weapon, weaponid)
{
	new skin = get_weapon_skin(weapon);

	//new name[32];get_user_name(id,name,charsmax(name))
	//server_print("[%s][Deploy]Skin %d, wid=%d", name, skin,weaponid)
	if(skin)
	{
		new data[Items];
		ArrayGetArray(g_Items, skin == ZERO_SKIN_INT ? 0 : skin, data);
		
		set_pev(id, pev_viewmodel2, data[ITEM_MODEL_V]);
		set_pev(id, pev_weaponmodel2, data[ITEM_MODEL_P]);
	}
}
public cs_weapon_drop(id, weaponbox, weapon, weaponid)
{
	new skin = get_weapon_skin(weapon);
	if(skin)
	{
		new data[Items];
		ArrayGetArray(g_Items, skin == ZERO_SKIN_INT ? 0 : skin, data);
		
		engfunc(EngFunc_SetModel, weaponbox, data[ITEM_MODEL_W]);
		return 1;
	}
	return 0;
}